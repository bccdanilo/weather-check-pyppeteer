"""
Extra code to scrap using pyppeteer
The ideia is to create a web browser 
and get the data after the AJAX loads
Please check the readme
"""
import asyncio
import re
import pyppeteer


async def get_site_info():
    """
    Function main to load the browser
    get the endpoint and
    return the content
    """
    endpoint = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"
    try:
        # Starting web browser headless
        browser = await pyppeteer.launch(options={'args': ['--no-sandbox']})
        # load site in the web browser
        site_content = await browser.newPage()
        await site_content.goto(endpoint)

        # wait to fully load site content - So AJAX can change
        await asyncio.sleep(3)
        weer_site = await site_content.content()
        await browser.close()

        # Return full site content
        return weer_site
    except (pyppeteer.errors.PageError, pyppeteer.errors.TimeoutError) as err:
        print("Pyppeteer Module Error", err)

    except Exception as err:
        print("Other Exception:", err)
    return None


def main():
    """
    Main method of this code, it will only print the required text
    """
    site = asyncio.get_event_loop().run_until_complete(get_site_info())
    if site:
        temperature = re.search(r'Currently: ([\d.]+)', site)
        print(f"{round(float(temperature.group(1)))} degrees Celcius")
    else:
        print("There was an issue on this method")


if __name__ == "__main__":
    main()
